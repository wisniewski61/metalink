package org.metalink.ant;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileModel {
	//TODO: md5hash, url
	private String name;
	private long size;
	private String md5hash;
	private String url;
	private String location;
	private String absolutePath;
	
	public FileModel(String file) {
		location = file;
		File f = new File(file);
		name = f.getName();
		size = f.length();
		absolutePath = f.getAbsolutePath();
	}
	
	public FileModel(String file, String baseUrl) {
		this(file);
		setUrl(baseUrl);
	}
	
	public void setUrl(String baseUrl) {
		String locationTemp = location.replace('\\', '/');
		if(baseUrl.charAt(baseUrl.length()-1)!='/')
			baseUrl += '/';
		url = baseUrl + locationTemp;
	}
	
	public void calcMd5Hash() {
		// source: https://stackoverflow.com/questions/304268/getting-a-files-md5-checksum-in-java/304350#304350
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			try (InputStream is = Files.newInputStream(Paths.get(location));
			     DigestInputStream dis = new DigestInputStream(is, md)) 
			{
				byte[] buffer = new byte[1024];
				while(dis.read(buffer)!=-1);
			}
			byte[] digest = md.digest();
			md5hash = toHexString(digest);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}//catch
	}//calcMd5Hash()
	
	
	// source: https://stackoverflow.com/questions/332079/in-java-how-do-i-convert-a-byte-array-to-a-string-of-hex-digits-while-keeping-l/332101#332101
	private String toHexString(byte[] bytes) {
	    StringBuilder hexString = new StringBuilder();

	    for (int i = 0; i < bytes.length; i++) {
	        String hex = Integer.toHexString(0xFF & bytes[i]);
	        if (hex.length() == 1) {
	            hexString.append('0');
	        }
	        hexString.append(hex);
	    }

	    return hexString.toString();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("name: ")
				.append(name).append("\nsize: ").append(size)
				.append("\nlocation: ").append(location)
				.append("\nabsolute path: ").append(absolutePath);
		if(url!=null)
			sb.append("\nURL: ").append(url);
		if(md5hash!=null)
			sb.append("\nMD5: ").append(md5hash);
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public long getSize() {
		return size;
	}

	public String getMd5hash() {
		return md5hash;
	}

	public String getUrl() {
		return url;
	}

	public String getLocation() {
		return location;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}
	
}
