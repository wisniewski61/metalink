package org.metalink.ant;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.wisniewski.xml.XmlDocumentBuilder;
import org.wisniewski.xml.XmlNode;
import org.wisniewski.xml.XmlNodeException;

public class Meta4 extends Task{
	private String url;
	private String file;
	private FileSet fileset;
	private final String defaultUrlPropertyName = "server.files.url";
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
	
	public void addFileset(FileSet fileset) {
		this.fileset = fileset;
	}
	
	public void execute() {
		if(this.url==null)
			this.url = getProject().getProperty(defaultUrlPropertyName);
		System.out.println("Reading files...");
		DirectoryScanner ds = fileset.getDirectoryScanner();
		String[] files = ds.getIncludedFiles();
		ArrayList<FileModel> includedFilesModels = new ArrayList<FileModel>();
		
		for (int i = 0; i < files.length; i++) {
			//System.out.println(files[i]);
			includedFilesModels.add(new FileModel(files[i], url));
		}//for
		System.out.println("Found "+ includedFilesModels.size() + " files");
		System.out.println("Calculating MD5 hashes...");
		for(FileModel fm : includedFilesModels) {
			fm.calcMd5Hash();
		}
		
		System.out.println("Writing xml...");
		writeXml(includedFilesModels);		
		System.out.println("Finished");
	}//execute()
	
	private void writeXml(ArrayList<FileModel> includedFilesModels) {
		XmlNode metalink = new XmlNode("metalink");
		metalink.addAttribute("xmlns", "urn:ietf:params:xml:ns:metalink");
		XmlNode published = new XmlNode("published", new Date().toString());
		try {
			metalink.addChild(published);
			for(FileModel fm : includedFilesModels) {
				XmlNode fileNode = new XmlNode("file");
				fileNode.addAttribute("name", fm.getName());
				//size
				fileNode.addChild(new XmlNode("size", String.valueOf(fm.getSize())));
				//hash
				XmlNode hash = new XmlNode("hash", fm.getMd5hash());
				hash.addAttribute("type", "md5");
				fileNode.addChild(hash);
				//url
				fileNode.addChild(new XmlNode("url", fm.getUrl()));
				
				metalink.addChild(fileNode);
			}//for
		} catch (XmlNodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		XmlDocumentBuilder xml = new XmlDocumentBuilder(metalink);
		try(PrintWriter out = new PrintWriter(new File(file), "UTF-8")) {
		    out.println(xml);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println("Dawid Wi�niewski 2017 - Metalink.\n\nThis is metalink ant task. Run it from ant script.");
	}
}
