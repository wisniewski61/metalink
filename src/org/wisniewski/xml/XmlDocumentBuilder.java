package org.wisniewski.xml;

public class XmlDocumentBuilder {
	private String version;
	private String encoding;
	private XmlNode content;
	
	public XmlDocumentBuilder(XmlNode rootElement) {
		this();
		this.content = rootElement;
	}
	
	public XmlDocumentBuilder() {
		version = "1.0";
		encoding = "UTF-8";
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"").append(version).append("\" encoding=\"")
			.append(encoding).append("\"?>");
		
		if(content!=null) {
			sb.append("\n").append(content);
		}
		return sb.toString();
	}
	
	public XmlNode getContent() {
		return content;
	}
	public void setContent(XmlNode content) {
		this.content = content;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getEncoding() {
		return encoding;
	}
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
