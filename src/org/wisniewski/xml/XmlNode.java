package org.wisniewski.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class XmlNode {
	private String name;
	private HashMap<String, String> attributes;
	private ArrayList<XmlNode> childs;
	private String content;
	
	public XmlNode(String name) {
		this.name = name;
		this.attributes = new HashMap<>();
		this.childs = new ArrayList<>();
	}
	
	public XmlNode(String name, String textContent) {
		this(name);
		this.content = textContent;
	}
	
	public void addAttribute(String key, String value) {
		if(key.length()==0)
			throw new IllegalArgumentException("Key cannot be empty");
		this.attributes.put(key, value);
	}
	
	public void addChild(XmlNode child) throws XmlNodeException {
		if(content!=null)
			throw new XmlNodeException("You can't add child to text XML node!");
		this.childs.add(child);
	}
	
	public void setContent(String text) {
		this.content = text;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<").append(name);
		if(!attributes.isEmpty()) {
			for(Entry<String, String> entry : this.attributes.entrySet()) {
				sb.append(" ").append(entry.getKey()).append("=\"").append(entry.getValue())
				.append("\"");
			}//for
		}//if
		sb.append(">");
		
		if(content!=null) {
			sb.append(content);
		} else {
			sb.append("\n");
			for(XmlNode child : this.childs)
				sb.append(child);
		}//else
		
		sb.append("</").append(name).append(">\n");
		return sb.toString();
	}
}
