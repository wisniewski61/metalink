package org.wisniewski.xml;

public class XmlNodeException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7595320611263502639L;

	public XmlNodeException(String message) {
		super(message);
	}
}
